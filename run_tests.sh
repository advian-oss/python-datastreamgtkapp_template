#!/bin/bash -x
test -t 1 && USE_TTY="-it"
# Make sure our tools are installed and paths set
cookiecutter --version || pip install cookiecutter
poetry --version || curl -sSL https://install.python-poetry.org | POETRY_VERSION="1.2.2" python3 -
export PATH="/root/.local/bin:$PATH"
# Start aborting on failed commands
set -e
old_workdir=$(pwd)
tmp_dir=$(mktemp -d "/tmp/ccgtktestbuild.XXXXXXXX")
cd "$tmp_dir"
cookiecutter -f --no-input "$old_workdir"
cd python-nameless
if [ ! -L Dockerfile ]; then
  exit 1
fi

if [ "$(uname -s )" == "Darwin" ]; then
  export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"
else
  export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"
fi

# Init repo so it looks like a real project
git init
git checkout -b cctest
POETRY_VIRTUALENVS_PATH=$tmp_dir poetry lock
git add .
git commit -am 'automated tests'

# We need buildkit
export DOCKER_BUILDKIT=1
# Docker tests
docker build --progress plain --ssh default  --target test -t ccgtktestbuild:test_alpine -f Dockerfile_alpine .
# shellcheck disable=SC2086
docker run $USE_TTY --rm $DOCKER_SSHAGENT -v "$(pwd):/app" ccgtktestbuild:test_alpine /bin/bash -c "source /root/.profile && poetry lock && poetry env remove --all"
git add poetry.lock
# shellcheck disable=SC2086
docker run $USE_TTY --rm $DOCKER_SSHAGENT -v "$(pwd):/app" -e "SKIP=check-executables-have-shebangs" ccgtktestbuild:test_alpine
docker build --progress plain --ssh default --target test -t ccgtktestbuild:test_ubuntu -f Dockerfile_ubuntu .
# shellcheck disable=SC2086
docker run $USE_TTY --rm $DOCKER_SSHAGENT -v "$(pwd):/app" -e "SKIP=check-executables-have-shebangs" ccgtktestbuild:test_ubuntu
# Docker devel shell
docker build --progress plain --ssh default --target devel_shell -t ccgtktestbuild:devel_shell_alpine -f Dockerfile_alpine .
# shellcheck disable=SC2086
docker run $USE_TTY --rm -v "$(pwd):/app" ccgtktestbuild:devel_shell_alpine -c "source /root/.zshrc && SKIP=check-executables-have-shebangs pre-commit run --all-files"
docker build --progress plain --ssh default --target devel_shell -t ccgtktestbuild:devel_shell_ubuntu -f Dockerfile_ubuntu .
# shellcheck disable=SC2086
docker run $USE_TTY --rm -v "$(pwd):/app" ccgtktestbuild:devel_shell_ubuntu -c "source /root/.zshrc && SKIP=check-executables-have-shebangs pre-commit run --all-files"
# make sure git-up is there
# shellcheck disable=SC2086
docker run $USE_TTY --rm ccgtktestbuild:devel_shell_alpine -c "source /root/.zshrc && which git-up"
# shellcheck disable=SC2086
docker run $USE_TTY --rm ccgtktestbuild:devel_shell_ubuntu -c "source /root/.zshrc && which git-up"
# Docker production image
docker build --progress plain --ssh default --target production -t ccgtktestbuild:production_alpine -f Dockerfile_alpine .
# shellcheck disable=SC2086
docker run $USE_TTY --rm ccgtktestbuild:production_alpine true
docker build --progress plain --ssh default --target production -t ccgtktestbuild:production_ubuntu -f Dockerfile_ubuntu .
# shellcheck disable=SC2086
docker run $USE_TTY --rm ccgtktestbuild:production_ubuntu true

rm -rf "$tmp_dir"
