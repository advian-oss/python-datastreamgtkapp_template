# python-datastreamgtkapp_template

Cookiecutter template for GTK applications using https://gitlab.com/advian-oss/python-gobjectservicelib

Non-GUI applications that need GObject can use https://gitlab.com/advian-oss/python-datastreamserviceapp_template
and just change the package the baseclasses are imported from.

## Usage

  1. Install [cookiecutter](https://pypi.org/project/cookiecutter/)
  2. `cookiecutter gl:advian-oss/python-datastreamgtkapp_template`
  3. Fill in the forms
  4. Profit!

The newly generated directory structure has more information, remember init your repo first.
