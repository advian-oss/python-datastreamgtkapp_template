#!/usr/bin/env python
import os
import subprocess
import tempfile

os.symlink("Dockerfile_alpine", "Dockerfile")

with tempfile.TemporaryDirectory() as tmpdirname:
    print("Attempting to run 'poetry lock'")
    subprocess.run(f"POETRY_VIRTUALENVS_PATH={tmpdirname} poetry lock", shell=True, check=False, capture_output=False, encoding='utf-8')

print("Attempting to run 'git init && git add .'")
subprocess.run('git init && git add .', shell=True, check=False, capture_output=False, encoding='utf-8')

print("""
*****************************************************
Created as {{ cookiecutter.repo_name }}

Check README.rst for further instructions

Especially the "Local Development" section at the end
*****************************************************
""")
