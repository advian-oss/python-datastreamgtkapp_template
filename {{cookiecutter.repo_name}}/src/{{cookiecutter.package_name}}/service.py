"""Main classes for {{cookiecutter.project_name}}"""

from __future__ import annotations
import asyncio
from typing import cast, Union, Any, List, Dict
from dataclasses import dataclass, field
import logging


from datastreamcorelib.pubsub import PubSubMessage, Subscription
from datastreamcorelib.datamessage import PubSubDataMessage
from gobjectservicelib.service import SimpleService
from gobjectservicelib.eventloop import gloop

from .gui import MainWindow

LOGGER = logging.getLogger(__name__)


# FIXME: Check this name is good (and PEP8 compliant)
# NOTE: SimpleService does implicit magic, if you don't want that: inherit from BaseService instead
@dataclass
class {{cookiecutter.package_name|capitalize}}Service(SimpleService):
    """Main class for {{cookiecutter.project_name}}"""

    _gtk_roots: Dict[str, Any] = field(init=False, default_factory=dict)

    async def setup(self) -> None:
        await super().setup()
        self._gtk_roots["window"] = MainWindow(self)

        def mainwindow_destroy(*args: List[Any]) -> None:
            """Handle the window close event, call quit"""
            nonlocal self
            LOGGER.debug("Called with {}".format(args))
            if not self._aioloop.is_closed():
                self._aioloop.call_soon_threadsafe(self.quit, 0)
            self._gtk_roots["window"] = None

        self._gtk_roots["window"].connect("destroy", mainwindow_destroy)

    @gloop
    def show_window(self) -> None:
        """Show the window to demonstrate the gloop decorator"""
        self._gtk_roots["window"].show_all()

    async def post_gloop_setup(self) -> None:
        """Setup the GUI base"""
        await super().post_gloop_setup()
        # Demonstrate the decorator, for trivial things like this using self._gloop.call_soon is actually better
        self.show_window()

    async def pre_gloop_teardown(self) -> None:
        """Called before self._gloop.quit"""
        if self._gtk_roots.get("window"):
            self._gloop.call_soon(self._gtk_roots["window"].destroy)

    def reload(self) -> None:
        """Load configs, restart sockets"""
        super().reload()
        # Do something

        # Example task sending messages forever
        self.create_task(self.example_message_sender("footopic"), name="example_sender")

        # Example subscription for receiving messages (specifically DataMessages)
        sub = Subscription(
            self.config["zmq"]["pub_sockets"][0],  # Listen to our own heartbeat
            "HEARTBEAT",
            self.example_message_callback,
            decoder_class=PubSubDataMessage,
            # This is just an example, don't pass metadata you don't intent to use
            metadata={"somekey": "somevalue"},
        )
        self.psmgr.subscribe_async(sub)

    async def example_message_callback(self, sub: Subscription, msg: PubSubMessage) -> None:
        """Callback for the example subscription"""
        # We know it's actually datamessage but the broker deals with the parent type
        msg = cast(PubSubDataMessage, msg)
        LOGGER.info("Got {} (sub.metadata={})".format(msg, sub.metadata))

        # Append the message to the main window textarea via thread-safe callback
        self._gloop.call_soon(self._gtk_roots["window"].textarea_insert, str(msg))

        outmsg = PubSubDataMessage(topic="bartopic")
        await self.psmgr.publish_async(outmsg)

    async def example_message_sender(self, topic: Union[bytes, str]) -> None:
        """Send messages in a loop, the topic is just an example for passing typed arguments"""
        msgno = 0
        try:
            while self.psmgr.default_pub_socket and not self.psmgr.default_pub_socket.closed:
                msgno += 1
                msg = PubSubDataMessage(topic=topic)
                msg.data = {
                    "msgno": msgno,
                }
                LOGGER.debug("Publishing {}".format(msg))
                await self.psmgr.publish_async(msg)
                await asyncio.sleep(0.5)
        except asyncio.CancelledError:
            # handle cancellations grafefully
            pass
        return None
