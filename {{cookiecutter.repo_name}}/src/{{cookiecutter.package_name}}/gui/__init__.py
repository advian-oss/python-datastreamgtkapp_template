"""The GUI stuff separated to a submodule"""

from __future__ import annotations
from typing import cast, List, Any
import asyncio
import logging

import gi  # type: ignore

gi.require_version("Gtk", "3.0")
# pylint: disable=C0413
from gi.repository import Gtk  # type: ignore
from datastreamcorelib.datamessage import PubSubDataMessage
from gobjectservicelib.service import SimpleService
from gobjectservicelib.widgets import ServiceWindowBase
from gobjectservicelib.eventloop import aio

LOGGER = logging.getLogger(__name__)


class MainWindow(ServiceWindowBase):
    """Root of our GUI"""

    def __init__(self, service: SimpleService, *args: Any, **kwargs: Any) -> None:
        """Initialize"""
        kwargs["title"] = "{{cookiecutter.package_name}} main"
        super().__init__(service, *args, **kwargs)
        self.set_default_size(600, 350)
        self.grid = Gtk.Grid()
        self.add(self.grid)  # pylint: disable=E1101

        self.create_textview()
        self.create_toolbar()

    @aio
    def button1_click(self, *args: List[Any]) -> None:
        """create task to publish the event, demonstrates the @aio decorator to run the callback
        in the asyncio event loop instead of GObject event loop where GUI events normally run"""
        LOGGER.debug("Called with {}".format(args))
        self._service = cast(SimpleService, self._service)
        psmgr = self._service.psmgr

        async def pub_task() -> None:
            """publish the event"""
            nonlocal psmgr
            msg = PubSubDataMessage(topic="button1_click")
            await psmgr.publish_async(msg)

        self._service.create_task(pub_task())

    def create_toolbar(self) -> None:
        """Create the toolbar with buttons"""
        toolbar = Gtk.Toolbar()
        self.grid.attach(toolbar, 0, 0, 3, 1)

        btn = Gtk.ToolButton()
        btn.set_icon_name("network-transmit-symbolic")
        toolbar.insert(btn, 0)
        btn.connect("clicked", self.button1_click)

    def textarea_insert(self, insert_row: str) -> None:
        """Insert a new row to end of the textarea"""
        end_iter = self.textbuffer.get_end_iter()
        self.textbuffer.insert(end_iter, "{}\n".format(insert_row))

    def create_textview(self) -> None:
        """Add scrollable textview to the main grid"""
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        self.grid.attach(scrolledwindow, 0, 1, 3, 1)

        self.textview = Gtk.TextView()
        self.textbuffer = self.textview.get_buffer()
        scrolledwindow.add(self.textview)  # pylint: disable=E1101
