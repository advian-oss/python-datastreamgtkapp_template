"""Default configuration file contents"""

# Remember to add tests for keys into test_{{cookiecutter.package_name}}.py
DEFAULT_CONFIG_STR = """
[zmq]
pub_sockets = ["ipc:///tmp/{{cookiecutter.package_name}}_pub.sock", "tcp://*:{{ range(49152, 65535) | random }}"]
""".lstrip()
