=============================
{{cookiecutter.project_name}}
=============================

!!!! READ ME FIRST: Start by going to the "Local Development" section at end of this document !!!!

!!!! FIXME: You MUST edit this file, read it through *with thought* !!!!

!!!! FIXME: Check CI config file, should you change the repo pointer ?? !!!!

!!!! FIXME: Check pyroject.toml: repo pointer, license, authors etc !!!!

{{ cookiecutter.project_short_description }}

Docker
------

This depends on GObject and GTK libraries etc from the operating system level, easiest way
to get hacking is to build the docker image and work inside it.

Alpine is the recommended base, the images are half the size but if you need Ubuntu there's
and example Dockerfile for that too. Remove the one you are not using and update the symlink
if needed.

Remember to add any new system packages you install via the devel_shell to the Dockerfile too.
Easy way to check if you remembered everything is to rebuild the test image and run it (see below).


SSH agent forwarding
^^^^^^^^^^^^^^^^^^^^

We need buildkit_::

    export DOCKER_BUILDKIT=1

.. _buildkit: https://docs.docker.com/develop/develop-images/build_enhancements/

And also the exact way for forwarding agent to running instance is different on OSX::

    export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"

and Linux::

    export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"

Passing X11 to Docker on OSX
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You need XQuartz (brew cask install xquartz), and since the actual Docker engine runs in a
VM you need to allow connections over network (disabled by default, enable in settings and restart
XQuartz), this you need to do only once.

Then you need authorize connections from localhost (xhost + 127.0.0.1), this you need to do after every XQuartz
restart. Finally you need to set the DISPLAY ENV variable correctly when creating the container (done below),
for copy-pasting commands as-is export the following::

    export DOCKER_DISPLAY="-v $HOME/.Xauthority:/root/.Xauthority:rw -e DISPLAY=host.docker.internal:0"

Passing X11 to Docker on Linux
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here we can get away with just passing the DISPLAY and XAUTHORITY env variables as-is (you still need to have those
exported so run these from a GUI terminal or know more about what you are doing), but we do need to use host networking
again for copy-pasting commands as-is export the following::

    export DOCKER_DISPLAY="-v $XAUTHORITY:/root/.Xauthority:rw --env=DISPLAY --net=host"

Creating the container
^^^^^^^^^^^^^^^^^^^^^^

Make sure you have defined DOCKER_DISPLAY above.

!!! FIXME switch the 1234 port(s) to the port from src/{{cookiecutter.package_name}}/defaultconfig.py !!!

Build image, create container and start it::

    docker build --progress plain --ssh default --target devel_shell -t {{cookiecutter.package_name}}:devel_shell .
    docker create --name {{cookiecutter.package_name}}_devel -p 1234:1234 -v "$(pwd):/app" -v /tmp:/tmp -it $(echo $DOCKER_SSHAGENT) $(echo $DOCKER_DISPLAY) {{cookiecutter.package_name}}:devel_shell
    docker start -i {{cookiecutter.package_name}}_devel

This will give you a shell with system level dependencies installed, you should do any shell things (like
run tests, pre-commit checks etc) there.

pre-commit considerations
^^^^^^^^^^^^^^^^^^^^^^^^^

If working in Docker instead of native env you need to run the pre-commit checks in docker too::

    docker exec -i {{cookiecutter.package_name}}_devel /bin/bash -c "pre-commit install"
    docker exec -i {{cookiecutter.package_name}}_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above. Or alternatively use the docker run syntax but using
the running container is faster::

    docker run --rm -v "$(pwd):/app" {{cookiecutter.package_name}}:devel_shell -c "pre-commit run --all-files"

Test suite
^^^^^^^^^^

You can use the devel shell to run py.test when doing development, for CI use
the "test" target in the Dockerfile::

    docker build --ssh default --target test -t {{cookiecutter.package_name}}:test .
    docker run --rm -v "$(pwd):/app" `echo DOCKER_SSHAGENT` {{cookiecutter.package_name}}:test

Production docker
^^^^^^^^^^^^^^^^^

!!! FIXME switch the 1234 port(s) to the port from src/{{cookiecutter.package_name}}/defaultconfig.py !!!

There's a "production" target as well for running the application, make sure your have the DOCKER_DISPLAY
ENV variable set correctly and remember to change that architecture tag to arm64 if building on ARM::

    docker build --progress plain --ssh default --target production -t {{cookiecutter.package_name}}:amd64-latest .
    docker run --rm -it --name {{cookiecutter.package_name}}_config {{cookiecutter.package_name}}:amd64-latest {{cookiecutter.package_name}} --defaultconfig >config.toml
    docker run --rm --name {{cookiecutter.package_name}} -v "$(pwd)/config.toml:/app/docker_config.toml" -p 1234:1234 -it -v /tmp:/tmp $(echo $DOCKER_DISPLAY) {{cookiecutter.package_name}}:amd64-latest


Local Development
-----------------

!!! FIXME: Remove the repo init from this document after you have done it. !!!

NOTE: Unless you're running Linux you are probably better off using the devel_shell from Docker
and even then you may need to install some extra system level libraries.

TLDR:

- Create and activate a Python 3.11 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p $(which python3.11) my_virtualenv

- Init your repo (first create it on-line and make note of the remote URI)::

    git init && git add .  # This should have been done automatically by cookiecutter
    git commit -m 'Cookiecutter stubs'
    git remote add origin MYREPOURI
    git branch -m main
    git push origin main

- change to a branch::

    git checkout -b my_branch

- install Poetry: https://python-poetry.org/docs/#installation
- Install project deps and pre-commit hooks::

    poetry install
    git add poetry.lock
    pre-commit install
    pre-commit run --all-files

If you get weird errors about missing packages from pre-commit try running it with "poetry run pre-commit".

- Ready to go, try the following::

    {{cookiecutter.package_name}} --defaultconfig >config.toml
    {{cookiecutter.package_name}} -vv config.toml

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).

Running "pre-commit run --all-files" and "py.test -v" regularly during development and
especially before committing will save you some headache.

Monitoring ZMQ messgaes
^^^^^^^^^^^^^^^^^^^^^^^

Datastreamservicelib that we depend on provides a tool called testsubscriber::

    testsubscriber -s ipc:///tmp/{{cookiecutter.package_name}}_pub.sock -t "HEARTBEAT"

set -t (--topic) to the prefix you want to filter for. If you expect ImageMessages add "-i" to make life less binary.

Pro tip: -t "" give you ALL messages published in the socket, it can be useful but it is generally overwhelming.
