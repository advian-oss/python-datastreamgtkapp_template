"""Test fixtures"""

from typing import AsyncGenerator
import asyncio
import logging
from pathlib import Path
import platform
import random

import tomlkit
import pytest
import pytest_asyncio
from libadvian.testhelpers import nice_tmpdir  # pylint: disable=W0611
from libadvian.logging import init_logging
from datastreamservicelib.compat import asyncio_eventloop_check_policy, asyncio_eventloop_get

from {{cookiecutter.package_name}}.defaultconfig import DEFAULT_CONFIG_STR
from {{cookiecutter.package_name}}.service import {% set serviceclass = cookiecutter.package_name|capitalize + "Service" %}{{ serviceclass }}


# pylint: disable=W0621
init_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)
asyncio_eventloop_check_policy()


@pytest.fixture
def event_loop():  # type: ignore
    """override pytest-asyncio default eventloop"""
    loop = asyncio_eventloop_get()
    LOGGER.debug("Yielding {}".format(loop))
    yield loop
    loop.close()


@pytest_asyncio.fixture
async def service_instance(nice_tmpdir: str) -> {{ serviceclass }}:
    """Create a service instance for use with tests"""
    parsed = tomlkit.parse(DEFAULT_CONFIG_STR).unwrap()
    # On platforms other than windows, do not bind to TCP (and put the sockets into temp paths/ports)
    pub_sock_path = "ipc://" + str(Path(nice_tmpdir) / "{{cookiecutter.package_name}}_pub.sock")
    if platform.system() == "Windows":
        pub_sock_path = f"tcp://127.0.0.1:{random.randint(1337, 65000)}"  # nosec
    parsed["zmq"]["pub_sockets"] = [pub_sock_path]
    # Write a testing config file
    configpath = Path(nice_tmpdir) / "{{cookiecutter.package_name}}_testing.toml"
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        fpntr.write(tomlkit.dumps(parsed))
    # Instantiate service and return it
    serv = {{ serviceclass }}(configpath)
    return serv


@pytest_asyncio.fixture
async def running_service_instance(service_instance: {{ serviceclass }}) -> AsyncGenerator[{{ serviceclass }}, None]:
    """Yield a running service instance, shut it down after the test"""
    task = asyncio.create_task(service_instance.run())
    # Yield a moment so setup can do it's thing
    await asyncio.sleep(0.1)

    yield service_instance

    service_instance.quit()

    try:
        await asyncio.wait_for(task, timeout=2)
    except TimeoutError:
        task.cancel()
    finally:
        # Clear alarms and default exception handlers
        {{ serviceclass }}.clear_exit_alarm()
        asyncio.get_event_loop().set_exception_handler(None)
